import java.util.Scanner;

/**
 * Función Main
 */

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcion = pedirOpcion(scanner);
        comprobarLasOpciones(opcion);
    }

    static void comprobarLasOpciones(int opcion) {

        Scanner scanner = new Scanner(System.in);
        if (opcion == 1) {
            suma(scanner);
        } else if (opcion == 2) {
            resta(scanner);
        } else if (opcion == 3) {
            multiplicacion(scanner);
        } else if (opcion == 4) {
            division(scanner);
        } else {
            System.out.println("Has elegido una opcion erronea");
        }


    }


    static int pedirOpcion(Scanner scanner) {

        System.out.println("Que opción quieres elegir");
        System.out.println("Pulsa 1 para sumar");
        System.out.println("Pulsa 2 para restar");
        System.out.println("Pulsa 3 para multiplicar");
        System.out.println("Pulsa 4 para dividir");

        return scanner.nextInt();
    }

    static void suma(Scanner scanner) {

        System.out.println("Introduce los números para sumar");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        number1 += number2;
        System.out.println("El resultado de la suma es " + number1);

    }

    static void resta(Scanner scanner) {
        System.out.println("Introduce los números para restar");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        number1 -= number2;
        System.out.println("El resultado de la suma es " + number1);

    }

    static void multiplicacion(Scanner scanner) {
        System.out.println("Introduce los números para multiplicar");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        number1 *= number2;
        System.out.println("El resultado de la multiplicación es " + number1);


    }

    static void division(Scanner scanner) {
        System.out.println("Introduce los números para dividir");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        number1 /= number2;
        System.out.println("El resultado de la división es " + number1);


    }

}

